## GDScript Transpiler Properties Class

class_name Keyword

const KW_NONE: int = 0
const KW_CLASSNAME: int = -1
const KW_EXTENDS: int = -2
const KW_NUMBERSIGN2: int = -3
const KW_FUNCTION: int = -4
const KW_NEW: int = -5
const KW_VARIABLE: int = -6
const KW_CONST: int = -7
const KW_FOR: int = -8
const KW_IN: int = -9
const KW_IF: int = -10
